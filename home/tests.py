from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import signin, signout, signup, landing
from django.contrib.auth.models import User

# Create your tests here.


class SignInTest(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/signin/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/signin/')
        self.assertEqual(found_func.func, signin)

    def test_event_using_template(self):
        template = Client().get('/signin/')
        self.assertTemplateUsed(template, 'home/signin.html')

    def test_signin_with_new_account(self):
        c = Client()

        user = User.objects.create(
            username='andhikalfariz', email='andhikalfariz@gmail.com')
        user.set_password('dikadika123123')
        user.save()

        logged_in = c.login(username='andhikalfariz',
                            password='dikadika123123')

        self.assertTrue(logged_in)


class SignUpTest(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/signup/')
        self.assertEqual(found_func.func, signup)

    def test_event_using_template(self):
        template = Client().get('/signup/')
        self.assertTemplateUsed(template, 'home/signup.html')


class SignOutTest(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/signout/')
        self.assertEqual(response.status_code, 302)

    def test_event_func(self):
        found_func = resolve('/signout/')
        self.assertEqual(found_func.func, signout)
